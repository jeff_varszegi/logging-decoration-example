﻿using System;
using LoggingDecorationExample.Logging;

namespace LoggingDecorationExample
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Demo of setting the same logger on two log-source objects and successfully logging messages

            ILogger logger = new ConsoleLogger();
            
            SomeObject o1 = new SomeObject("one");
            SomeObject o2 = new SomeObject("two");
            o1.SetLogger(logger);
            o2.SetLogger(logger);

            o1.SomeMethod();
            o2.SomeMethod();

            #endregion

            #region Generation of basic timing info

            System.Console.WriteLine(Environment.NewLine + "Getting timing on overhead of logging calls using extension methods...");

            SomeObject o3 = new SomeObject("three");
            o3.SetLogger(new NullLogger());
            string message = "";

            int operationCount = 100000000;
            DateTime startTime, endTime;
            double milliseconds;

            startTime = DateTime.Now;
            for (int x = 0; x < operationCount; x++)
                o3.Log(message);
            endTime = DateTime.Now;

            milliseconds = ((double)(endTime.Ticks - startTime.Ticks)) / 10000.0D;
            System.Console.WriteLine(milliseconds + " milliseconds elapsed");
            System.Console.WriteLine(((milliseconds * 1000000.0D) / ((double)operationCount)) + " nanoseconds per operation");

            #endregion

            Console.ReadLine();
        }
    }
}
