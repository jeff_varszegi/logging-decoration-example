﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggingDecorationExample.Logging
{
    /// <summary>Logs messages to some output</summary>
    public interface ILogger
    {
        /// <summary>Logs the specified message</summary>
        /// <param name="message">The message to log</param>
        void Log(string message);
    }
}
