﻿using System;
using System.Runtime.CompilerServices;

namespace LoggingDecorationExample.Logging
{
    /// <summary>Provides method implementations for ILogSource objects</summary>
    public static class ILogSourceExtensions
    {
        ///<summary>Stores extended data for objects</summary>
        private static ConditionalWeakTable<object, ILogger> loggerMap = new ConditionalWeakTable<object, ILogger>();

        /// <summary>Sets the specified logger for this log-message source</summary>
        /// <param name="logSource">this log source</param>
        /// <param name="logger">The logger to set</param>
        public static void SetLogger(this ILogSource logSource, ILogger logger) 
        {
            if (logSource == null || logger == null) return; // or handle as otherwise appropriate

            try
            {
                loggerMap.Add(logSource, logger);
            }
            catch (Exception e)
            {
                // Handle already-added loggers as appropriate (swap or throw exception, etc.)
            }
        }

        /// <summary>Gets the logger for this log-message source</summary>
        /// <param name="logSource">this log source</param>
        /// <returns>The ILogger, if any, set on this log source, otherwise null</returns>
        private static ILogger GetLogger(this ILogSource logSource)
        {   
            if (logSource == null) return null; // or handle as otherwise appropriate

            ILogger logger;
            if (loggerMap.TryGetValue(logSource, out logger))
                return logger;
            else
                return null;
        }

        /// <summary>Logs the specified message</summary>
        /// <param name="message">The message to log</param>
        public static void Log(this ILogSource logSource, string message) 
        {
            if (logSource == null) return; // or handle otherwise as appropriate
            else if (string.IsNullOrWhiteSpace(message)) return;

            ILogger logger = logSource.GetLogger();
            if (logger != null)
                logger.Log(message);
        }
    }

    /// <summary>A source of log messages</summary>
    public interface ILogSource {}
}
