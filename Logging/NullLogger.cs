﻿using System;

namespace LoggingDecorationExample.Logging
{
    /// <summary>An ILogger implementation which does nothing</summary>
    public class NullLogger : ILogger
    {
        /// <summary>Does nothing</summary>
        /// <param name="message">The message to ignore</param>
        public void Log(string message) { }
    }
}
