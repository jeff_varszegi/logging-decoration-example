﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggingDecorationExample.Logging
{
    /// <summary>An ILogger implementation which logs to the system console</summary>
    public class ConsoleLogger : ILogger
    {
        /// <summary>Logs the specified message</summary>
        /// <param name="message">The message to log</param>
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}
