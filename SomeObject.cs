﻿using System;

using LoggingDecorationExample.Logging;

namespace LoggingDecorationExample
{
    /// <summary>Some object which may generate log messages</summary>
    public class SomeObject : ILogSource
    {
        /// <summary>This object's name</summary>
        public string Name { get; private set; }

        /// <summary>Default constructor</summary>
        private SomeObject() { }

        /// <summary>Constructs a new instance</summary>
        /// <param name="name">This object's</param>
        public SomeObject(string name)
        {
            Name = name;
        }

        /// <summary>Does something useful</summary>
        public void SomeMethod()
        {
            this.Log(Name + ".SomeMethod() start...");

            // Do something useful

            this.Log(Name + ".SomeMethod() end");
        }
    }
}
